<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmakerCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmaker_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bookmaker_game_id');
            $table->unsignedInteger('user_id');
            $table->float('factor');
            $table->decimal('cost',30,2);
            $table->boolean('result')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bookmaker_game_id')->references('id')->on('bookmaker_games');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmaker_cart');
    }
}
