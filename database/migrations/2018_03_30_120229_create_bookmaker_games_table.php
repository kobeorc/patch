<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmakerGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmaker_games', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('factor_id');
            $table->unsignedInteger('team_one_id');
            $table->unsignedInteger('team_two_id');
            $table->integer('result_one')->nullable();
            $table->integer('result_two')->nullable();
            $table->dateTime('start_at');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('factor_id')->references('id')->on('factors');
            $table->foreign('team_one_id')->references('id')->on('teams');
            $table->foreign('team_two_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmaker_games');
    }
}
