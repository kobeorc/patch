<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('w1');
            $table->string('x');
            $table->string('w2');
            $table->string('w1x');
            $table->string('w2x');
            $table->string('w12');
            $table->string('f1');
            $table->string('f2');
            $table->string('f1_value');
            $table->string('f2_value');
            $table->string('t_m');
            $table->string('t_l');
            $table->string('t_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factors');
    }
}
