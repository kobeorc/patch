<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmakerBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmaker_bets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bookmaker_game_id');
            $table->unsignedInteger('bookmaker_cart_id');
            $table->string('bet');
            $table->boolean('result')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bookmaker_game_id')->references('id')->on('bookmaker_games');
            $table->foreign('bookmaker_cart_id')->references('id')->on('bookmaker_carts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmaker_bets');
    }
}
