<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Team;

class TeamsSeeder extends Seeder
{
    public function run()
    {
        factory(Team::class,5)->create()->each(function ($u){
            $u->category()->associate(Category::inRandomOrder()->first() ?? factory(Category::class)->create());
        });
    }
}