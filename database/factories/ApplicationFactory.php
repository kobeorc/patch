<?php

use Faker\Generator as Faker;
use \App\Models\Application;
use \App\Models\User;

$factory->define(Application::class, function (Faker $faker) {
    return [
        'user_id'   => function(){
            return User::inRandomOrder()->first() ?? factory(User::class)->create();
        },
        'console'       => array_random(Application::$available_console),
        'time_start'    => $faker->dateTimeBetween('now','+ 2 days'),
        'time_end'      => $faker->dateTimeBetween('+3 days','+5 days'),
        'gold_min'      => random_int(1,200),
        'gold_max'      => random_int(200,5000)
    ];
});
