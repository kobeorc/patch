<?php

use Faker\Generator as Faker;
use \App\Models\BookmakerBet;
use \App\Models\BookmakerCart;
use \App\Models\BookmakerGame;

$factory->define(BookmakerBet::class, function (Faker $faker) {
    return [
        'bookmaker_game_id' => function(){
            return BookmakerGame::inRandomOrder()->first() ?? factory(BookmakerGame::class)->create();
        },
        'bookmaker_cart_id' => function(){
            return BookmakerCart::inRandomOrder()->first() ?? factory(BookmakerCart::class)->create();
        },
        'bet'   => array_random(BookmakerBet::$bookmaker_bet)
    ];
});
