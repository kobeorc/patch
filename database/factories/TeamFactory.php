<?php

use Faker\Generator as Faker;
use App\Models\Team;
use App\Models\Category;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'name'          => $faker->word,
        'date_of_start' => $this->faker->dateTimeInInterval('-1 days'),
        'category_id'   => function(){
            return Category::inRandomOrder()->first() ?? factory(Category::class)->create();
        }
    ];
});
