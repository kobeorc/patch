<?php

use Faker\Generator as Faker;
use \App\Models\BookmakerCart;
use \App\Models\User;
use \App\Models\BookmakerGame;

$factory->define(BookmakerCart::class, function (Faker $faker) {
    return [
        'bookmaker_game_id' => function(){
            return BookmakerGame::inRandomOrder()->first() ?? factory(BookmakerGame::class)->create();
        },
        'user_id'           => function(){
            return User::inRandomOrder()->first() ?? factory(User::class)->create();
        },
        'factor'            => $faker->randomFloat(2,1,50),
        'cost'              => $faker->randomDigit
    ];
});