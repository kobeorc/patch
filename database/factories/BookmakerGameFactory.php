<?php

use Faker\Generator as Faker;
use App\Models\BookmakerGame;
use App\Models\Category;
use App\Models\Factor;
use App\Models\Team;

$factory->define(BookmakerGame::class, function (Faker $faker) {
    $teams = Team::inRandomOrder()->limit(2)->get();
    return [
        'category_id'   => function(){
            return Category::inRandomOrder()->first() ?? factory(Category::class)->create();
        },
        'factor_id'     => function(){
            return factory(Factor::class)->create();
        },
        'team_one_id'   => function() use ($teams){
            return $teams->first() ?? factory(Team::class)->create();
        },
        'team_two_id'   => function() use ($teams){
            return $teams->last() ?? factory(Team::class)->create();
        },
        'result_one'    => null,
        'result_two'    => null,
        'start_at'      => $faker->dateTimeInInterval('1 days'),
    ];
});