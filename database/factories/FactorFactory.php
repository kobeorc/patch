<?php

use Faker\Generator as Faker;
use App\Models\Factor;

$factory->define(Factor::class, function (Faker $faker) {
    return [
        'w1'        => $faker->randomFloat(2,1,20),
        'x'         => $faker->randomFloat(2,1,20),
        'w2'        => $faker->randomFloat(2,1,20),
        'w1x'       => $faker->randomFloat(2,1,20),
        'w2x'       => $faker->randomFloat(2,1,20),
        'w12'       => $faker->randomFloat(2,1,20),
        'f1'        => $faker->randomFloat(2,1,20),
        'f2'        => $faker->randomFloat(2,1,20),
        'f1_value'  => $faker->randomFloat(2,1,20),
        'f2_value'  => $faker->randomFloat(2,1,20),
        't_m'       => $faker->randomFloat(2,1,20),
        't_l'       => $faker->randomFloat(2,1,20),
        't_value'   => $faker->randomFloat(2,1,20)
    ];
});
