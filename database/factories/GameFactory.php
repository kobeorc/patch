<?php

use Faker\Generator as Faker;
use App\Models\Game;
use App\Models\Application;
use App\Models\User;

$factory->define(Game::class, function (Faker $faker) {
    return [
        'application_id'    => function(){
            return Application::inRandomOrder()->first() ?? factory(Application::class)->create();
        },
        'creator_id'        => function(){
            return User::inRandomOrder()->first() ?? factory(User::class)->create();
        },
        'taker_id'          => function(){
            return User::inRandomOrder()->first() ?? factory(User::class)->create();
        },
        'accept'            => rand(0,1),
        'start_at'          => function(array $current) use ($faker){
            $application = Application::find($current['application_id'])->first();
            return $application->exists() ?
                \Carbon\Carbon::createFromTimestampUTC(random_int($application->time_start->timestamp,$application->time_end->timestamp)) :
                '2017-10-10 10:10:10';
        },
        'gold'              => function(array $current) use ($faker){
            $application = Application::find($current['application_id'])->first();
            return $application->exists() ? random_int($application->gold_min,$application->gold_max) : $faker->randomNumber(3);
        }
    ];
});