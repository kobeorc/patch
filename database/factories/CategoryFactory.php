<?php

use Faker\Generator as Faker;
use App\Models\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'parent_id' => function(){
            return rand(1,0) ? factory(Category::class)->create() : null;
        }
    ];
});
