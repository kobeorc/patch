<?php

namespace App\Console\Commands;

use App\Models\BookmakerCart;
use App\Models\BookmakerGame;
use Illuminate\Console\Command;

class BookmakerCartEvents extends Command
{
    protected $signature = 'bookmaker_cart:run';
    protected $description = 'run bookmaker cart events';
    protected $count;

    public function __construct()
    {
        parent::__construct();
        $this->count = 10;
    }

    public function handle()
    {
        if(BookmakerGame::count() < $this->count || BookmakerCart::count() < $this->count){
            $this->autoCreating();
        }
    }

    protected function autoCreating()
    {
        factory(BookmakerCart::class,$this->count)->create();
    }
}
