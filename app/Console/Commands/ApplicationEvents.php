<?php

namespace App\Console\Commands;

use App\Models\Application;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ApplicationEvents extends Command
{
    protected $signature = 'application:run';
    protected $description = 'Auto_create events';
    protected $count;

    public function __construct()
    {
        parent::__construct();
        $this->count = 10;
    }

    public function handle()
    {
        $this->autoDeleting();
        if(Application::count() < $this->count)
            $this->autoCreating();
    }

    protected function autoCreating()
    {
        \factory(Application::class,$this->count)->create();
    }

    protected function autoDeleting()
    {
        $applications = Application::where('time_end','<',Carbon::now()->addDays(2))->get();
        foreach ($applications as $application) {
            $application->delete();
        }
    }
}
