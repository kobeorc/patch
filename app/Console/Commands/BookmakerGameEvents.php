<?php

namespace App\Console\Commands;

use App\Events\BookmakerGameDoneEvent;
use App\Models\BookmakerGame;
use Carbon\Carbon;
use Illuminate\Console\Command;

class BookmakerGameEvents extends Command
{
    protected $signature = 'bookmaker_game:run';
    protected $description = 'run bookmaker game events';
    private $count,
        $categories,
        $teams,
        $faker;

    public function __construct()
    {
        parent::__construct();
        $this->count = 10;
    }

    public function handle()
    {
        $this->autoCalculateResult();
        $this->autoDeleting();
        if(BookmakerGame::count() < $this->count)
            $this->autoCreating();
    }

    protected function autoCreating()
    {
        \factory(BookmakerGame::class,$this->count)->create();
    }

    protected function autoDeleting()
    {
        /** @var BookmakerGame $games */
        $games = BookmakerGame::where('start_at','<',Carbon::now()->addDays(2))->get();
        foreach ($games as $game){
            $game->delete();
        }
    }

    protected function autoCalculateResult()
    {
        /** @var BookmakerGame $games */
        $games = BookmakerGame::notCompleted()->get();
        foreach ($games as $game){
            $game->result_one = random_int(1,10);
            $game->result_two = random_int(1,10);
            $game->save();
        }
    }
}
