<?php

namespace App\Console\Commands;

use App\Models\BookmakerBet;
use App\Models\BookmakerGame;
use Illuminate\Console\Command;

class BookmakerBetEvents extends Command
{
    protected $signature = 'bookmaker_bet:run';
    protected $description = 'run bookmaker bet events';
    protected $count;

    public function __construct()
    {
        parent::__construct();
        $this->count = 10;
    }

    public function handle()
    {
        if(BookmakerGame::count() < $this->count || BookmakerBet::count() < $this->count)
            $this->autoCreating();
    }

    protected function autoCreating()
    {
        factory(BookmakerBet::class,20)->create();
    }
}
