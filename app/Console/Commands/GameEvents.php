<?php

namespace App\Console\Commands;

use App\Models\Game;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GameEvents extends Command
{
    protected $signature = 'game:run';
    protected $description = 'rum game events';
    protected $count;

    public function __construct()
    {
        parent::__construct();
        $this->count = 10;
    }

    public function handle()
    {
        $this->autoDeleting();
        if(Game::count() < $this->count)
            $this->autoCreating();
    }

    protected function autoCreating()
    {
        /** @var Application $applications */
        \factory(Game::class,5)->create();
    }

    protected function autoDeleting()
    {
        /** @var Game $games */
        $games = Game::where('start_at','<',Carbon::now()->addDays(2))->get();

        foreach ($games as $game) {
            $game->delete();
        }
    }
}
