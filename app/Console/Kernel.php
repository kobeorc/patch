<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [];

    protected function schedule(Schedule $schedule)
    {
        // auto generate items in db
        if(env('APP_DEBUG')){
            $schedule->command('application:run')->everyMinute();
            $schedule->command('game:run')->everyMinute();
            $schedule->command('bookmaker_game:run')->everyMinute();
            $schedule->command('bookmaker_cart:run')->everyMinute();
            $schedule->command('bookmaker_bet:run')->everyMinute();
        }
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
