<?php

namespace App\Http\Controllers;

use App\Http\Resources\ApplicationResource;
use App\Models\Application;


class HomeController extends Controller
{
    public function index()
    {
        return ApplicationResource::collection(Application::paginate());
    }
}
