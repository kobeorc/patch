<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\GameResource;
use App\Models\Game;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    const PAGINATE_COUNT = 10;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return GameResource::collection(Game::paginate(self::PAGINATE_COUNT));
    }

    public function create(){}
    public function store(Request $request){}

    /**
     * @param Game $game
     * @return GameResource
     */
    public function show(Game $game)
    {
        return new GameResource($game);
    }

    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}
