<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicationController extends Controller
{
    const PAGINATE_COUNT = 10;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ApplicationResource::collection(Application::paginate(self::PAGINATE_COUNT));
    }

    public function create(){}
    public function store(Request $request){}

    /**
     * @param Application $application
     * @return ApplicationResource
     */
    public function show(Application $application)
    {
        return new ApplicationResource($application);
    }

    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}