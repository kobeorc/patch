<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ApplicationResource extends Resource
{

    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'user'          => $this->user,
            'time_start'    => $this->time_start,
        ];
    }
}
