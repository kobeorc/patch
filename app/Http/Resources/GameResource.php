<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GameResource extends Resource
{

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
