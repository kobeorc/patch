<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookmakerBet
 *
 * @property int $id
 * @property int $bookmaker_game_id
 * @property int $bookmaker_cart_id
 * @property string $bet
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\BookmakerCart $bookmaker_cart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereBet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereBookmakerCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereBookmakerGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $result
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet completed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerBet notCompleted()
 */
class BookmakerBet extends Model
{
    public static $bookmaker_bet = [
        'w1','x','w2','w1x','w2x','w12','f1','f2','t_m','t_l'
    ];
    public function bookmaker_cart()
    {
        return $this->belongsTo(BookmakerCart::class);
    }
    public function scopeNotCompleted(Builder $query)
    {
        return $query->whereNull('result');
    }
    public function scopeCompleted(Builder $query)
    {
        return $query->whereNotNull('result');
    }
    public function setResultFalseAndSave()
    {
        $this->result = false;
        return $this->save();
    }
    public function setResultTrueAndSave()
    {
        $this->result = true;
        return $this->save();
    }
}
