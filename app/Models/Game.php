<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Game
 *
 * @property int $id
 * @property int $application_id
 * @property int $creator_id
 * @property int $taker_id
 * @property int $accept
 * @property string $start_at
 * @property int $gold
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Application $application
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Game onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereGold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereTakerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Game whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Game withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Game withoutTrashed()
 * @mixin \Eloquent
 */
class Game extends Model
{
    use SoftDeletes;

    protected $dates = ['start_at'];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}
