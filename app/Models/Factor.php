<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Factor
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $w1
 * @property string $x
 * @property string $w2
 * @property string $w1x
 * @property string $w2x
 * @property string $w12
 * @property string $f1
 * @property string $f2
 * @property string $f1_value
 * @property string $f2_value
 * @property string $t_m
 * @property string $t_l
 * @property string $t_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereF1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereF1Value($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereF2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereF2Value($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereTL($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereTM($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereTValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereW1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereW12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereW1x($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereW2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereW2x($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factor whereX($value)
 */
class Factor extends Model
{
    //
}
