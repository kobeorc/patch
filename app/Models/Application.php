<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Application
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $console
 * @property string $time_start
 * @property string $time_end
 * @property int $gold_min
 * @property int $gold_max
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereConsole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereGoldMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereGoldMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereUserId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $games
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application withoutTrashed()
 */
class Application extends Model
{
    use SoftDeletes;

    protected $dates = ['time_start', 'time_end'];

    public static $available_console = ['ps','xbox'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function games()
    {
        return $this->hasMany(Game::class);
    }
}
