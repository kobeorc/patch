<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\BookmakerCart
 *
 * @property int $id
 * @property int $bookmaker_game_id
 * @property int $user_id
 * @property float $factor
 * @property float $cost
 * @property int $result
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookmakerBet[] $bookmaker_bet
 * @property-read \App\Models\BookmakerGame $bookmaker_game
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerCart onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereBookmakerGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereFactor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerCart withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerCart withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart betsCompleted()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart completed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerCart notCompleted()
 */
class BookmakerCart extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bookmaker_game()
    {
        return $this->belongsTo(BookmakerGame::class);
    }
    public function bookmaker_bet()
    {
        return $this->hasMany(BookmakerBet::class);
    }
    public function scopeCompleted(Builder $query)
    {
        return $query->whereNotNull('result');
    }
    public function scopeNotCompleted(Builder $query)
    {
        return $query->whereNull('result');
    }
    public function scopeBetsCompleted(Builder $query)
    {
        //TODO check
//        return $query->bookmaker_bet(function ($query){
//            return $query->completed();
//        });
    }
}
