<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\BookmakerGame
 *
 * @property int $id
 * @property int $category_id
 * @property int $factor_id
 * @property int $team_one_id
 * @property int $team_two_id
 * @property int|null $result_one
 * @property int|null $result_two
 * @property string $start_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereFactorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereResultOne($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereResultTwo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereTeamOneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereTeamTwoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\Factor $factor
 * @property-read \App\Models\User $team_one
 * @property-read \App\Models\User $team_two
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerGame onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerGame withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BookmakerGame withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookmakerCart[] $bookmaker_cart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame completed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookmakerGame notCompleted()
 */
class BookmakerGame extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function factor()
    {
        return $this->belongsTo(Factor::class);
    }
    public function team_one()
    {
        return $this->belongsTo(User::class);
    }
    public function team_two()
    {
        return $this->belongsTo(User::class);
    }
    public function bookmaker_cart()
    {
        return $this->hasMany(BookmakerCart::class);
    }

    /* Scopes */
    public function scopeNotCompleted(Builder $query)
    {
        return $query->whereNull('result_one')->whereNull('result_two');
    }
    public function scopeCompleted(Builder $query)
    {
        return $query->whereNotNull('result_one')->whereNotNull('result_two');
    }
}
