<?php

namespace App\Listeners;

use App\Bookmaker\CalculateWinFactor;
use App\Events\BookmakerGameDoneEvent;
use App\Models\BookmakerBet;
use App\Models\BookmakerGame;

class UpdateBookmakerBetStatusListener
{

    public function handle(BookmakerGameDoneEvent $event)
    {
        /** @var BookmakerGame $game */
        if($event->game instanceof BookmakerGame){
           foreach(BookmakerBet::notCompleted()->whereBookmakerGameId($event->game->id)->get() as $bet){
               $result_factor = (new CalculateWinFactor($event->game))->getWinnerFactors();
               in_array($bet->bet,$result_factor) ?
                   $bet->setResultTrueAndSave() :
                   $bet->setResultFalseAndSave();
           }
        }
    }
}