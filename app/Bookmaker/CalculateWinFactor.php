<?php

namespace App\Bookmaker;

use App\Models\BookmakerGame;

class CalculateWinFactor
{
    private
        $bookmaker_game,
        $factor,
        $team_one,
        $team_two,
        $win_factors;

    public function __construct(BookmakerGame $bookmaker_game)
    {
        $this->bookmaker_game   = $bookmaker_game;
        $this->factor           = $bookmaker_game->factor;
        $this->team_one = $bookmaker_game->result_one;
        $this->team_two = $bookmaker_game->result_two;
    }

    protected function calculateWinnerFactors()
    {
        switch ($this->team_one <=> $this->team_two){
            case 0:
                $this->win_factors[] = ['x','w1x','w2x'];
                break;
            case -1:
                $this->win_factors[] = ['w2','w2x','w12'];
                break;
            case  1:
                $this->win_factors[] =  ['w1','w1x','w12'];
                break;
        }
        return $this->win_factors;
    }

    protected function calculateHandicap()
    {
        if(($this->team_one - $this->team_two) >= $this->factor->f1_value){
            $this->win_factors[] = ['f1'];
        }elseif(($this->team_two - $this->team_one) >= $this->factor->f2_value){
            $this->win_factors[] = ['f2'];
        }
    }

    protected function calculateTotal()
    {
        switch (($this->team_one + $this->team_two) <=> $this->factor->t_value){
            case -1:
                $this->win_factors[] = ['t_l'];
                break;
            case 1:
                $this->win_factors[] = ['t_m'];
                break;
        }
    }

    public function getWinnerFactors()
    {
        $this->calculateHandicap();
        $this->calculateTotal();
        $this->calculateWinnerFactors();

        return array_collapse($this->win_factors);
    }
}