<?php
/**
 * Created by PhpStorm.
 * User: starcode
 * Date: 09.04.18
 * Time: 15:22
 */

namespace App\Events;

use App\Models\BookmakerGame;

class BookmakerGameDoneEvent
{
    public $game,$kappa;

    public function __construct(BookmakerGame $game)
    {
        $this->game = $game;
    }
}